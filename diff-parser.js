#!/usr/bin/env node

const parse = require('diffparser');

function serializeHunks(hunk) {
  return [hunk.content, ...hunk.changes.map(x => x.content)].join('\n');
}

function serialize(file) {
  const { from, to } = file;

  const ce = from === '/dev/null' ? `a/${to}` : `a/${from}`;
  const ce2 = from === '/dev/null' ? from : `a/${from}`;
  const ee = to === '/dev/null' ? `b/${from}` : `b/${to}`;
  const ee2 = to === '/dev/null' ? to : `b/${to}`;

  return [
    `diff --git ${ce} ${ee}`,
    `index ${file.index.join(' ')}`,
    `--- ${ce2}`,
    `+++ ${ee2}`,
    ...file.chunks.map(serializeHunks),
  ].join('\n');
}

function isEmptyLine(line) {
  // Consider unchanged lines empty
  if (line.type === 'normal') {
    return true;
  }

  const trimmed = line.content.trim();

  // Consider lines with whitespace changes only empty
  if (trimmed === '+' || trimmed === '-') {
    return true;
  }

  // Consider lines with ruby comments empty
  if (/^[+-]\s*#/.test(line.content)) {
    return true;
  }

  // Consider lines with prepend / include / extend of EE modules empty
  return /(prepend|include|extend)(\s+|\()(::)?EE/.test(line.content);
}

function isEmptyChunk(chunk) {
  return chunk.changes.every(isEmptyLine);
}

const content = [];
process.stdin.resume();
process.stdin.on('data', function(buf) {
  content.push(buf);
});

process.stdin.on('end', function() {
  // your code here
  const parsed = parse(Buffer.concat(content).toString('utf8'));

  const filtered = parsed
    .filter(file => !file.binary)
    .filter(file => file.additions > 0 || file.deletions > 0)
    .filter(file => !file.chunks.every(isEmptyChunk));

  if (process.argv.includes('--details')) {
    const _ = require('lodash');
    const grouped = _(filtered)
      .map(diff => ({
        file: diff.to === '/dev/null' ? diff.from : diff.to,
        add: diff.additions,
        del: diff.deletions,
        serialized: serialize(diff),
      }))
      .groupBy(diff =>
        diff.file
          .split('/')
          .slice(0, -1)
          .join('/')
      )
      .toPairs()
      .sortBy(([key, value]) => -value.length)
      .fromPairs()
      .value();
    console.log(JSON.stringify(grouped));
  } else {
    const { add, rem } = filtered.reduce(
      ({ add, rem }, x) => {
        return { add: add + x.additions, rem: rem + x.deletions };
      },
      { rem: 0, add: 0 }
    );
    console.log([filtered.length, add, rem].join(' '));
  }
});
