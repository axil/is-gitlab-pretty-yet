# Is GitLab pretty yet?

This repo runs several script to track our progress on improving the Code Quality of GitLab:

- eslint: https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/eslint/
- prettier: https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/prettier/
- CE/EE diff: https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/diff/

The pages are generated about [every 4 hours](https://gitlab.com/leipert-projects/is-gitlab-pretty-yet/pipeline_schedules).

## Prerequisites

- git
- jq
- bash
- gzip
- awk
- curl
- node

## Local Vue Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Generate page

```bash
# generate static pages
$ bash ./analysis.sh
```
