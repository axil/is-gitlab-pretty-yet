import axios from 'axios';
import moment from 'moment';

function percent(pretty, total) {
  return Math.round((pretty / total) * 10000) / 100;
}

export const retrievePrettierMetadata = key => {
  return axios.get('./' + key + '/metadata.json').then(function(response) {
    const project = response.data;

    project.percent = percent(project.pretty, project.total);
    project.url = 'https://gitlab.com/gitlab-org/gitlab-' + project.version;
    project.commitUrl = project.url + '/tree/' + project.commit;
    project.name = 'GitLab ' + project.version.toLocaleUpperCase();
    project.commitTime = new Date(project.time * 1000);
    project.files = [];

    return project;
  });
};

export const retrieveEslintMetadata = key => {
  return axios
    .get('./' + key + '/eslint-metadata.json')
    .then(function(response) {
      const project = response.data;

      project.percent = percent(project.pretty, project.total);
      project.url = 'https://gitlab.com/gitlab-org/gitlab-' + project.version;
      project.commitUrl = project.url + '/tree/' + project.commit;
      project.name = 'GitLab ' + project.version.toLocaleUpperCase();
      project.commitTime = new Date(project.time * 1000);
      project.files = [];

      return project;
    });
};

const minSize = 5;
const maxSize = 25;

function scale(position, maxValue) {
  return Math.max(
    Math.round((Math.log(position) / Math.log(maxValue)) * maxSize),
    minSize
  );
}

const retrieveFilesForPrettier = key => {
  return axios.get('./' + key + '/files.txt').then(function(response) {
    let files = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const add = parseInt(chunks[0], 10);
        const rem = parseInt(chunks[1], 10);
        const dim = scale(add + rem, 500);

        files.push({
          add: add,
          rem: rem,
          name: chunks[3],
          dim: dim,
          pretty: chunks[2] === 'true',
          title: chunks[3] + '\n' + add + ' Insertions\n' + rem + ' Deletions',
        });
      }

      files = files.sort(function(a, b) {
        return b.dim - a.dim;
      });
    });

    return files;
  });
};

const retrieveFilesForEslint = key => {
  return axios.get('./' + key + '/eslint-files.txt').then(function(response) {
    let files = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const errors = parseInt(chunks[0], 10);
        const warnings = parseInt(chunks[1], 10);
        const dim = scale(errors + warnings, 300);

        files.push({
          errors: errors,
          warnings: warnings,
          name: chunks[2],
          class: errors ? 'red' : warnings ? 'yellow' : 'green',
          dim: dim,
          title:
            chunks[2] + '\n' + errors + ' Errors\n' + warnings + ' Warnings',
        });
      }

      files = files.sort(function(a, b) {
        return b.dim - a.dim;
      });
    });

    return files;
  });
};

export const retrieveFiles = (key, target) => {
  switch (target) {
    case 'eslint':
      return retrieveFilesForEslint(key);
    case 'prettier':
      return retrieveFilesForPrettier(key);
    default:
      throw new Error(`retrieveFiles: Unknown target ${target}`);
  }
};

const retrieveHistoryForPrettier = key => {
  return axios.get('./' + key + '/history.txt').then(function(response) {
    const percentage = [];
    const totalLines = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const timestamp = moment(parseInt(chunks[0], 10) * 1000);
        const totalFiles = parseInt(chunks[1], 10);
        const prettyFiles = parseInt(chunks[2], 10);
        const lineDiff = parseInt(chunks[3], 10);

        const data = {
          timestamp: timestamp,
          prettyFiles: prettyFiles,
          totalFiles: totalFiles,
          lineDiff: lineDiff,
          afterBody: [
            'Total files: ' + totalFiles,
            'Pretty files: ' + prettyFiles,
            'Commit: ' + chunks[4].substring(0, 10),
          ],
        };

        if (totalFiles > 0) {
          percentage.push({
            x: timestamp,
            y: percent(prettyFiles, totalFiles),
            data: data,
          });
          totalLines.push({ x: timestamp, y: lineDiff, data: data });
        }
      }
    });

    return {
      percentage,
      totalLines,
    };
  });
};

const retrieveHistoryForEslint = key => {
  return axios.get('./' + key + '/eslint-history.txt').then(function(response) {
    const percentage = [];
    const warnings = [];
    const errors = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const timestamp = moment(parseInt(chunks[0], 10) * 1000);
        const totalFiles = parseInt(chunks[1], 10);
        const prettyFiles = parseInt(chunks[2], 10);
        const errorCount = parseInt(chunks[3], 10);
        const warningsCount = parseInt(chunks[4], 10);

        const data = {
          timestamp: timestamp,
          prettyFiles: prettyFiles,
          totalFiles: totalFiles,
          errors: errorCount,
          warnings: warningsCount,
          afterBody: [
            'Total files: ' + totalFiles,
            'Pretty files: ' + prettyFiles,
            'Commit: ' + chunks[5].substring(0, 10),
          ],
        };

        if (totalFiles > 0) {
          percentage.push({
            x: timestamp,
            y: percent(prettyFiles, totalFiles),
            data: data,
          });
          errors.push({ x: timestamp, y: errorCount, data: data });
          warnings.push({ x: timestamp, y: warningsCount, data: data });
        }
      }
    });

    return {
      percentage,
      warnings,
      errors,
    };
  });
};

const retrieveHistoryForDiff = () => {
  return axios.get('./diff/diff-history-v4.txt').then(function(response) {
    const diffFiles = [];
    const additions = [];
    const deletions = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const timestamp = moment(parseInt(chunks[0], 10) * 1000);
        const files = parseInt(chunks[1], 10);
        const adds = parseInt(chunks[2], 10);
        const rems = parseInt(chunks[3], 10);

        const data = {
          timestamp: timestamp,
          diffFiles: files,
          additions: adds,
          deletions: rems,
          afterBody: [
            'Commit CE: ' + chunks[4].substring(0, 10),
            'Commit EE: ' + chunks[5].substring(0, 10),
          ],
        };

        if (files > 0) {
          diffFiles.push({
            x: timestamp,
            y: files,
            data: data,
          });
          deletions.push({ x: timestamp, y: rems, data: data });
          additions.push({ x: timestamp, y: adds, data: data });
        }
      }
    });

    return {
      diffFiles,
      additions,
      deletions,
    };
  });
};

export const retrieveHistory = (key, target) => {
  switch (target) {
    case 'prettier':
      return retrieveHistoryForPrettier(key);
    case 'eslint':
      return retrieveHistoryForEslint(key);
    case 'diff':
      return retrieveHistoryForDiff();
    default:
      throw new Error(`retrieveHistory: Unknown target ${target}`);
  }
};

export const diffFileList = () => {
  return axios.get('./diff/diff-aggregated.json').then(function(response) {
    return Object.entries(response.data).map(e => ({
      path: e[0],
      files: e[1],
    }));
  });
};
