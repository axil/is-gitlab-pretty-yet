#!/bin/bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
set -o noglob

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PRETTIER_SCOPE="**/*.vue **/*.js **/*.scss"
DIFF_SELECTOR='. :!ee :!qa :!CHANGELOG-EE.md :!locale :!db'
CI_BUILD_DATE=$(date '+%s')
CI_PROJECT_URL="${CI_PROJECT_URL:-https://gitlab.com/leipert/is-gitlab-pretty-yet}"
CI_JOB_NAME="${CI_JOB_NAME:-generate_stats}"
IS_PRETTY="true"

mkdir -p repositories

function log {
>&2 printf "\\e[33m%s\\e[0m\\n" "$1"
}

function indent { sed 's/^/    /' 1>&2; }

function checkout {
  if [ -d "repositories/$VERSION" ] ; then
    cd "repositories/$VERSION"
    git reset --hard
    git pull origin master
  else
    git clone --single-branch "https://gitlab.com/gitlab-org/gitlab-$VERSION.git" "repositories/$VERSION"
    cd "repositories/$VERSION"
  fi
}

function set_vars {
  cd "repositories/$VERSION"
  HASH=$(git rev-parse HEAD)
  TMP_DIR="$DIR/tmp/$HASH"
  TIME=$(git show -s --format=%ct)
  RESULT_DIR="$DIR/results/$VERSION/$TIME"
  cd "$DIR"
  rm -rf "$TMP_DIR"
  mkdir -p "$TMP_DIR"
  mkdir -p "$RESULT_DIR"
}

function install {
  cd "repositories/$VERSION"
  yarn install --pure-lockfile
  cd "$DIR"
}

function prettier {
  cd "repositories/$VERSION"
  # shellcheck disable=SC2086
  node_modules/.bin/prettier --write $PRETTIER_SCOPE > "$TMP_DIR/prettier.txt"
  git diff --numstat | grep -Ev '^-' > "$TMP_DIR/numstat.txt" || touch "$TMP_DIR/numstat.txt"
  git diff --shortstat > "$TMP_DIR/shortstat.txt"
  git diff --dirstat=files,3,noncumulative | sort -r -n -k1,1 > "$TMP_DIR/dirstat.txt"
  git reset --hard
  cd "$DIR"
}

function eslint {
  cd "repositories/$VERSION"
  node_modules/.bin/eslint . --ext .js,.vue -f "$DIR/slim-json-formatter.js" \
    --no-inline-config --report-unused-disable-directives \
    > "$TMP_DIR/eslint.json" || echo ""
  cd "$DIR"
}

function analysis_prettier {
  rm -rf "$TMP_DIR/parsed.txt"
  touch "$TMP_DIR/parsed.txt"
  local match
  local dirstat
  local total
  local pretty
  local shortstat
  local lines
  while read -r line; do
    local filename
    local adds
    local rems
    local okay
    adds="0"
    rems="0"
    okay="true"
    filename=$(echo "$line" | cut -f1 -d" ")
    match=$(grep -m 1 "$filename" "$TMP_DIR/numstat.txt" || echo "")
    if [ -n "$match" ]; then
      adds=$(echo "$match" | cut -f1 -d$'\t')
      rems=$(echo "$match" | cut -f2 -d$'\t')
      okay="false"
      IS_PRETTY="false"
    fi
    printf "%s\\t%s\\t%s\\t%s\\n" "$adds" "$rems" "$okay" "$filename" >> "$TMP_DIR/parsed.txt"
  done < "$TMP_DIR/prettier.txt"
  sort -k3,3 -k1nr,1 -k2nr,2 "$TMP_DIR/parsed.txt" > "$RESULT_DIR/files.txt"
  dirstat=$(awk '{$1=$1};1' "$TMP_DIR/dirstat.txt" | jq --raw-input --slurp 'split("\n") | .[0:-1]')
  total=$(wc -l < "$TMP_DIR/prettier.txt")
  cut -d" " -f1 "$TMP_DIR/prettier.txt" | sort > "$TMP_DIR/A.txt"
  cut -f3 "$TMP_DIR/numstat.txt" | sort > "$TMP_DIR/B.txt"
  pretty=$(comm -23 "$TMP_DIR/A.txt" "$TMP_DIR/B.txt" | wc -l)
  shortstat=$(awk '{$1=$1};1' "$TMP_DIR/shortstat.txt")
  log "Results for gitlab-$VERSION"
  echo '{}' \
    | jq --arg total "$total" '.total = $total' \
    | jq --arg total "$pretty" '.pretty = $total' \
    | jq --arg 'hash' "$HASH" '.commit = $hash' \
    | jq --arg 'version' "$VERSION" '.version = $version' \
    | jq --arg 'time' "$TIME" '.time = $time' \
    | jq --argjson 'dirstat' "$dirstat" '.dirstat = $dirstat' \
    | jq --arg 'shortstat' "$shortstat" '.shortstat = $shortstat' \
    | tee "$RESULT_DIR/metadata.json"

  log "Downloading history from gitlab CI"

  curl --fail --location --silent --show-error \
    "$CI_PROJECT_URL/-/jobs/artifacts/master/raw/public/$VERSION/history.txt?job=$CI_JOB_NAME" \
    > "$TMP_DIR/history.txt"

  if ! grep -q "$HASH" "$TMP_DIR/history.txt" ; then
    lines=$(awk '{ sum += $1 + $2 } END { print sum + 0 }' "$TMP_DIR/numstat.txt")
    printf "%s\\t%s\\t%s\\t%s\\t%s\\n" "$CI_BUILD_DATE" "$total" "$pretty" "$lines" "$HASH" >> "$TMP_DIR/history.txt"
  fi

  sort -n -k1 "$TMP_DIR/history.txt" > "$RESULT_DIR/history.txt"

  tail -n 10 "$RESULT_DIR/history.txt"
}

function analysis_eslint {
  local pretty
  local total
  local top20
  local errors
  local warnings
  pretty=0

  log "Parsing eslint results"

  total='
    sort_by(.errorCount, .warningCount, .filePath) | reverse |
      .[] | [.errorCount, .warningCount, .filePath ] | map(tostring) |
      join("\t")
  '

  jq -r "$total" "$TMP_DIR/eslint.json" | sed "s#/.*repositories/$VERSION/##g" > "$RESULT_DIR/eslint-files.txt"

  log "Aggregating eslint results"

  # shellcheck disable=SC2016
  top20='
    [.[].messages[].ruleId] |
      reduce .[] as $i ({}; setpath([$i]; getpath([$i]) + 1) ) |
      to_entries |
      sort_by(.value) | reverse | .[0:20] |
      from_entries
  '

  top20=$(jq "$top20" "$TMP_DIR/eslint.json")

  total=$(wc -l < "$RESULT_DIR/eslint-files.txt")
  pretty=$(grep -c "^0\\t0" "$RESULT_DIR/eslint-files.txt")
  errors=$(awk '{ sum += $1 } END { print sum + 0 }' "$RESULT_DIR/eslint-files.txt")
  warnings=$(awk '{ sum += $2 } END { print sum + 0 }' "$RESULT_DIR/eslint-files.txt")

  log "Eslint summary"

  echo '{}' \
    | jq --arg total "$total" '.total = $total' \
    | jq --arg pretty "$pretty" '.pretty = $pretty' \
    | jq --arg errors "$errors" '.errors = $errors' \
    | jq --arg warnings "$warnings" '.warnings = $warnings' \
    | jq --arg 'hash' "$HASH" '.commit = $hash' \
    | jq --arg 'version' "$VERSION" '.version = $version' \
    | jq --arg 'time' "$TIME" '.time = $time' \
    | jq --argjson 'top20' "$top20" '.top20 = $top20' \
    | tee "$RESULT_DIR/eslint-metadata.json"

  log "Downloading history from gitlab CI"

  curl --fail --location --silent --show-error \
    "$CI_PROJECT_URL/-/jobs/artifacts/master/raw/public/$VERSION/eslint-history.txt?job=$CI_JOB_NAME" \
    > "$TMP_DIR/eslint-history.txt"

  if ! grep -q "$HASH" "$TMP_DIR/eslint-history.txt" ; then
    printf "%s\\t%s\\t%s\\t%s\\t%s\\t%s\\n" "$CI_BUILD_DATE" "$total" "$pretty" "$errors" "$warnings" "$HASH" >> "$TMP_DIR/eslint-history.txt"
  fi

  sort -n -k1 "$TMP_DIR/eslint-history.txt" > "$RESULT_DIR/eslint-history.txt"

  tail -n 10 "$RESULT_DIR/eslint-history.txt"

}

function historic_diff {
  local stats
  local CE_COMMIT
  local EE_COMMIT
  local epoch
  epoch=$(date -d "$1" '+%s')
  CE_COMMIT=$(git rev-list --max-count=1 --first-parent --until="$1" ce/master)
  EE_COMMIT=$(git rev-list --max-count=1 --first-parent --until="$1" origin/master)
  # shellcheck disable=SC2086
  stats=$(git -c "diff.renameLimit=2000" diff $CE_COMMIT...$EE_COMMIT -- $DIFF_SELECTOR | "$DIR/diff-parser.js")
  # shellcheck disable=SC2183 disable=SC2086
  printf "%s\\t%s\\t%s\\t%s\\t%s\\t%s\\n" "$epoch" $stats "$CE_COMMIT" "$EE_COMMIT"
}

function seed_history {
  local day
  local end
  local epoch
  end=$2
  day=$(date -u --iso-8601=seconds -d "$end - $1 days")
  while [[ "$day" < "$end" ]]; do
    historic_diff "$day"
    day=$(date -u --iso-8601=seconds -d "$day + 12 hours")
  done
}

function analysis_split {
  local tmpfile
  tmpfile="$TMP_DIR/diff-history-v4.txt"
  cd repositories/ee
  git remote rm ce || echo "Remote ce did not exist"
  git remote add -f ce ../ce

  log "Downloading history from gitlab CI"

  curl --fail --location --silent --show-error \
    "$CI_PROJECT_URL/-/jobs/artifacts/master/raw/public/diff/diff-history-v4.txt?job=$CI_JOB_NAME" \
    > "$tmpfile" || rm -rf "$tmpfile"

  if [[ ! -f "$tmpfile" ]]; then
    log "No history available. Seeding the history now"
    touch "$tmpfile"
    seed_history 180 "$(date -u --iso-8601=seconds)" | tee -a "$tmpfile"
  else
    end=$(head -n 1 "$tmpfile" | cut -f1 -d$'\t' | awk '{$1=$1};1')
    end=$(date -u --iso-8601=seconds -d "@$end")
    if [[ "2016-01-01T00:00:00+00:00" < "$end" ]]; then
      log "Generating 100 entries before $end"
      mv "$tmpfile" "$TMP_DIR/after.txt"
      seed_history 100 "$end" | tee -a "$tmpfile"
      cat "$TMP_DIR/after.txt" >> "$tmpfile"
    fi
  fi

  historic_diff "$(date -u --iso-8601=seconds)" >> "$tmpfile"

  log "Generating file diff aggregate"

  # shellcheck disable=SC2086
  git diff ce/master..origin/master -- $DIFF_SELECTOR | "$DIR/diff-parser.js" --details > "$TMP_DIR/diff-aggregated.json"

  RESULT_DIR="$DIR/static/diff"
  rm -rf "$RESULT_DIR"
  mkdir -p "$RESULT_DIR"
  mv "$tmpfile" "$RESULT_DIR"
  mv "$TMP_DIR/diff-aggregated.json" "$RESULT_DIR"

  tail -n 10 "$RESULT_DIR/diff-history-v4.txt"

}

function run_pipeline {
  log "Checking out gitlab-$VERSION"
  checkout 2>&1 | indent
  set_vars
  log "Installing dependencies"
  install 2>&1 | indent
  log "Running eslint"
  eslint 2>&1 | indent
  log "Formatting eslint results properly"
  analysis_eslint 2>&1 | indent
  log "Running prettier"
  prettier 2>&1 | indent
  log "Formatting prettier results properly"
  analysis_prettier 2>&1 | indent
  log "Copying results to public dir"
  mkdir -p "static/$VERSION"
  cp -r "$RESULT_DIR/." "static/$VERSION"
  log "Finished running analysis on gitlab-$VERSION"
}


log "Check if everything is alright"
yarn install 2>&1 | indent
yarn run lint 2>&1 | indent

HASH=
TMP_DIR=
RESULT_DIR=
TIME=

if [ "${CI:=false}" == "true" ]; then
  log "Overwriting build date"
  echo "{}" \
    | jq --argjson buildDate "$CI_BUILD_DATE" '.buildDate = $buildDate' \
    | jq --arg repoUrl "$CI_PROJECT_URL" '.repoUrl = $repoUrl' \
    | jq --argjson isPretty "$IS_PRETTY" '.isPretty = $isPretty' \
    > assets/data.json
else
  log "Not in CI environment, not overwriting build date"
fi

VERSION=ce
run_pipeline
VERSION=ee
run_pipeline
set_vars
log "Analyse CE/EE Split"
analysis_split 2>&1 | indent

cd "$DIR"

log "Copying static page and zipping"
yarn run generate 2>&1 | indent
find dist -type f -print0 | xargs -0 gzip -f -k
log "Finished build"
tree dist/ | indent
